import {inject} from 'aurelia-dependency-injection';
import SubdivisionSynopsisView from './subdivisionSynopsisView';
import iso31662 from 'iso-3166-2';

/**
 * @class {ListSubdivisionsWithAlpha2CountryCodeUseCase}
 */
@inject(iso31662) class ListSubdivisionsWithAlpha2CountryCodeUseCase {

    _iso31662:iso31662;

    constructor(iso31662:iso31662) {

        if (!iso31662) {
            throw new TypeError('iso31662 required');
        }
        this._iso31662 = iso31662;

    }

    /**
     *
     * @param {string} alpha2CountryCode
     * @returns {Promise.<Array>|*}
     */
    execute(alpha2CountryCode:string):Promise<Array> {

        const subdivisionList = [];
        for (var subdivisionCode in this._iso31662.data[alpha2CountryCode].sub) {

            const name =
                this._iso31662
                    .data[alpha2CountryCode]
                    .sub[subdivisionCode]
                    .name
                    // strip any 'helpful' text within parenthesis
                    .replace(/ *\([^)]*\) */g, '');

            const code =
                this._iso31662
                    .subdivision(subdivisionCode)
                    .regionCode;

            subdivisionList.push(
                new SubdivisionSynopsisView(
                    name,
                    code
                )
            );

        }

        subdivisionList.sort((a, b)=>a.name.localeCompare(b.name));

        return Promise.resolve(subdivisionList);

    }
}

export default ListSubdivisionsWithAlpha2CountryCodeUseCase;
    


