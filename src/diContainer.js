import {Container} from 'aurelia-dependency-injection';
import iso31662 from 'iso-3166-2';
import ListCountriesUseCase from './listCountriesUseCase';
import ListSubdivisionsWithAlpha2CountryCodeUseCase from './listSubdivisionsWithAlpha2CountryCodeUseCase'

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    constructor() {

        this._container = new Container();

        this._container.registerInstance(iso31662, iso31662);
        this._registerUseCases();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerUseCases() {

        this._container.autoRegister(ListCountriesUseCase);

        this._container.autoRegister(ListSubdivisionsWithAlpha2CountryCodeUseCase);

    }

}
