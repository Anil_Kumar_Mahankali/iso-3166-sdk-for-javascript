/**
 * @class {CountrySynopsisView}
 */
export default class CountrySynopsisView {

    _name:string;

    _alpha2Code:string;

    /**
     *
     * @param {string} name
     * @param {string} alpha2Code
     */
    constructor(name:string,
                alpha2Code:string) {

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        if (!alpha2Code) {
            throw new TypeError('alpha2Code required')
        }
        this._alpha2Code = alpha2Code;

    }

    /**
     * @returns {string}
     */
    get name() {
        return this._name;
    }

    /**
     * @returns {string}
     */
    get alpha2Code() {
        return this._alpha2Code;
    }

}
