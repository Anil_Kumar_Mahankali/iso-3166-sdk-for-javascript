import {inject} from 'aurelia-dependency-injection';
import CountrySynopsisView from './countrySynopsisView';
import iso31662 from 'iso-3166-2';

/**
 * @class {ListCountriesUseCase}
 */
@inject(iso31662) class ListCountriesUseCase {

    _countryList;

    constructor(iso31662:iso31662) {

        if (!iso31662) {
            throw new TypeError('iso31662 required');
        }

        this._countryList = [];
        for (var countryCode in iso31662.data) {

            const name = iso31662.data[countryCode].name;

            const alpha2Code = countryCode;

            this._countryList.push(
                new CountrySynopsisView(
                    name,
                    alpha2Code
                )
            );
        }

        this._countryList.sort((a, b)=>a.name.localeCompare(b.name));

    }

    /**
     * @returns {Promise.<CountrySynopsisView[]>}
     */
    execute():Promise<Array> {

        return Promise.resolve(this._countryList);

    }
}

export default ListCountriesUseCase;


