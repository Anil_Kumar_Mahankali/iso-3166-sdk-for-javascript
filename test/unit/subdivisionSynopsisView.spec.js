import SubdivisionSynopsisView from '../../src/subdivisionSynopsisView';
import dummy from '../dummy';

describe('SubdivisionSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if name is null', () => {
            /*
             arrange
             */
            const validCode = dummy.countryAlpha2Code;
            const constructor = () => new SubdivisionSynopsisView(null, validCode);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });
        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.countryName;
            const validCode = dummy.countryAlpha2Code;

            /*
             act
             */
            const objectUnderTest = new SubdivisionSynopsisView(expectedName, validCode);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
        it('throws if code is null', () => {
            /*
             arrange
             */
            const validName = dummy.countryName;
            const constructor = () => new SubdivisionSynopsisView(validName, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'code required');
        });

        it('sets code', () => {
            /*
             arrange
             */
            const expectedCode = dummy.countryAlpha2Code;
            const validName = dummy.countryName;

            /*
             act
             */
            const objectUnderTest = new SubdivisionSynopsisView(validName, expectedCode);

            /*
             assert
             */
            const actualCode = objectUnderTest.code;
            expect(actualCode).toEqual(expectedCode);

        });
    })
});
