import CountrySynopsisView from '../../src/countrySynopsisView';
import dummy from '../dummy';

describe('CountrySynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if name is null', () => {
            /*
             arrange
             */
            const validAlpha2Code = dummy.subdivisionCode;
            const constructor = () => new CountrySynopsisView(null, validAlpha2Code);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });
        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.subdivisionName;
            const validAlpha2Code = dummy.subdivisionCode;

            /*
             act
             */
            const objectUnderTest = new CountrySynopsisView(expectedName, validAlpha2Code);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
        it('throws if alpha2Code is null', () => {
            /*
             arrange
             */
            const validName = dummy.subdivisionName;
            const constructor = () => new CountrySynopsisView(validName, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'alpha2Code required');
        });

        it('sets alpha2Code', () => {
            /*
             arrange
             */
            const expectedAlpha2Code = dummy.subdivisionCode;
            const validName = dummy.subdivisionName;

            /*
             act
             */
            const objectUnderTest = new CountrySynopsisView(validName, expectedAlpha2Code);

            /*
             assert
             */
            const actualAlpha2Code = objectUnderTest.alpha2Code;
            expect(actualAlpha2Code).toEqual(expectedAlpha2Code);

        });
    })
});
