## Description
Precorconnect ISO 3166 SDK for javascript.

## Use Cases

#####List Countries
lists all countries.

#####List Subdivisions With Alpha 2 Country Code
lists all subdivisions with the provided alpha 2 country code

## Setup  

**install via jspm**  
```shell
jspm install bitbucket:precorconnect/iso-3166-sdk-for-javascript
``` 

**import & instantiate**
```js
import iso3166Sdk from 'iso-3166-sdk';
    
const iso3166Sdk = new iso3166Sdk();
```

## Platform Support

This library can be used in the **browser** or **node.js**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```